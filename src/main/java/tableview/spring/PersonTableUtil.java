package tableview.spring;

import java.time.LocalDate;

import com.sun.xml.internal.ws.fault.ServerSOAPFaultException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonTableUtil {

    @Autowired
    private PersonRepository personRepository;

    /* Returns an observable list of persons */
    public ObservableList<Person> getPersonList() {
        //Person p1 = new Person("Ashwin", "Sharan", LocalDate.of(2012, 10, 11));
        ObservableList<Person> people = FXCollections.observableArrayList();

        Iterable<Person> person = personRepository.findAll();
        person.forEach(people::add);

        return people;
    }

    /* Returns Person Id TableColumn */
    public TableColumn<Person, Integer> getIdColumn() {
        TableColumn<Person, Integer> personIdCol = new TableColumn<>("Id");
        personIdCol.setCellValueFactory(new PropertyValueFactory<>("personid"));
        return personIdCol;
    }

    /* Returns First Name TableColumn */
    public TableColumn<Person, String> getFirstNameColumn() {
        TableColumn<Person, String> fNameCol = new TableColumn<>("First Name");
        fNameCol.setCellValueFactory(new PropertyValueFactory<>("firstname"));
        return fNameCol;
    }

    /* Returns Last Name TableColumn */
    public TableColumn<Person, String> getLastNameColumn() {
        TableColumn<Person, String> lastNameCol = new TableColumn<>("Last Name");
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("lastname"));
        return lastNameCol;
    }

    /* Returns Birth Date TableColumn */
    public TableColumn<Person, String> getBirthDateColumn() {
        TableColumn<Person, String> bDateCol = new TableColumn<>("Birth Date");
        bDateCol.setCellValueFactory(new PropertyValueFactory<>("birthdate"));
        return bDateCol;
    }
}
