package tableview.spring;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Ryan on 2/18/2018.
 */
@Repository
public interface PersonRepository extends CrudRepository<Person, Integer> {
}
