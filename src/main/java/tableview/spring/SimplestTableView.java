package tableview.spring;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@SuppressWarnings("unchecked")
public class SimplestTableView extends Application {

    private Parent root;
    private ConfigurableApplicationContext springContext;

    @Autowired
    private PersonTableUtil personTableUtil;

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void init() throws Exception {
        springContext = SpringApplication.run(SimplestTableView.class);
        springContext.getAutowireCapableBeanFactory().autowireBean(this);
        super.init();
    }


    @Override
    public void start(Stage stage) {

        // Create a TableView with a list of persons
        TableView<Person> table = new TableView<>(personTableUtil.getPersonList());

        // Add columns to the TableView
        table.getColumns().addAll(personTableUtil.getIdColumn(),
                personTableUtil.getFirstNameColumn(),
                personTableUtil.getLastNameColumn(),
                personTableUtil.getBirthDateColumn());

        VBox root = new VBox(table);
        root.setStyle("-fx-padding: 10;" +
                "-fx-border-style: dotted inside;" +
                "-fx-border-width: 2;" +
                "-fx-border-insets: 5;" +
                "-fx-border-radius: 5;" +
                "-fx-border-color: red;");

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Simplest TableView");
        stage.show();
    }
}
