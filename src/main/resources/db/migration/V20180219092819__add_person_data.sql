INSERT INTO public.person(
	firstname, lastname, birthdate)
	VALUES ( 'Martha', 'Dober', '1965-01-25'),
	( 'Joe', 'Doe', '2001-07-16'),
	( 'Bud', 'Donnovan', '1998-12-05'),
	( 'John', 'Smith', '1945-08-29');