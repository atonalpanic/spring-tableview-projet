-- Table: public.person

-- DROP TABLE public.person;

CREATE TABLE public.person
(
    personid SERIAL PRIMARY KEY NOT NULL,
    firstname character varying(25),
    lastname character varying(25),
    birthdate date
)